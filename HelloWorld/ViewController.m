//
//  ViewController.m
//  HelloWorld
//
//  Created by Sword Software on 16/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}



-(IBAction)showMessage{
 
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"My First App"message:@"HEllo,World!"preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
   // [helloWorldAlert show];
    
    
}

@end
